var destApp = angular.module('destApp', ["ngRoute"]);
   destApp.config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/game',{
                templateUrl:'level.html',
                controller: 'destCtrl'
            })
            .when('/start',{
                templateUrl:'game.html'
            })
            .when('/alphabeth',{
                templateUrl:'alphabeth.html',
                controller: 'alphabethCtrl'
            })
            .when('/levels',{
                templateUrl:'levels.html',
                controller: 'alphabethCtrl'
            })
            .otherwise({
            redirectTo: '/start'
        });
        $locationProvider.html5Mode(true);
    });